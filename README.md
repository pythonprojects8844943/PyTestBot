# PyTestBot
It's an example Python Discord bot that you can host on your own computer and use.
## Features
The bot features four different commands.
- !help - responds with a list of these commands.
- !rnd - generates a random number between 2 numbers written. Does not support negative or decimal numbers.
- !timenow - responds with facts about the current time.
- !currency - responds with the current exchange rate between 2 currencies. Only accepts 3-letter codes - EUR, CZK, GBP, USD...
## Requirements
Python with version 3.10. Download from https://www.python.org
Upon installation, make sure to tick the checkbox for "**Add Python to PATH**", else the next steps will not work.
A Discord bot token is also needed. The bot will not work if it is verified, due to Discord's limitations.
## Installation
1. Open CMD by pressing Windows+R and typing in "cmd".
2. Type in `pip install discord.py`.
3. Type in `pip install requests`.
4. Open the bot.py file with any text editor or IDE.
5. Insert the bot token where it says so in the file.
6. Open CMD again and set your working directory to where bot.py is.
7. Type in `python bot.py`.
8. Your bot is now installed and running. Repeat step 7 if you close CMD, as the bot depends on the program to run.