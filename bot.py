import requests

import os

import discord

import random

# Insert your Discord bot token below.

TOKEN = "TOKEN"

intents = discord.Intents.default()
intents.members = True
intents.message_content = True
intents.dm_messages = True
intents.guild_messages = True
intents.emojis_and_stickers = True
intents.guilds = True
intents.integrations = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

@client.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(
        f'Vítej na serveru, uživateli {member.name}!'
    )

@client.event
async def on_message(message):

    if message.author == client.user:
        return

    if message.content.lower().startswith("!help"):
        await message.channel.send('Tohle je bot pro otestování funkce bota na Discordu. v0.3.0-beta\n**Příkazy:**\n- !help - zobrazí tuto zprávu.\n- !rnd a b - vygeneruje náhodné číslo od a do b.\n- !currency a b - vrátí měnový kurz mezi měnami a a b - ze stránky https://freecurrencyapi.com\n- !timenow - vrátí informace o čase.\nBot je zatím jenom prototyp pro testování komunikace s API, proto neexistují další příkazy.')
    if message.content.lower().startswith("!rnd"):
        a = ""
        b = ""
        c = 0
        for i in message.content.lower()[5:len(message.content.lower())]:
            if i.isnumeric() and c == 0:
                a = a + i
            elif i.isnumeric() and c == 1:
                b = b + i
            elif not i.isnumeric():
                c = 1
        print(a)
        print(b)
        num = random.randint(int(a), int(b))
        response = f"Vygenerované číslo: {num}"
        await message.channel.send(response)
    if message.content.lower().startswith("!currency"):
        url = "https://api.freecurrencyapi.com/v1/latest"
        base_currency = message.content.upper()[10:13]
        currencies = message.content.upper()[14:17]
        print(base_currency, currencies)
        params = {"apikey": "fca_live_bYacLEUV4yTuTE4i5DbLYpn1w5MmgdhNIhz3wCal", "base_currency": base_currency, "currencies": currencies}
        rq = requests.get(url = url, params = params)
        dat = rq.json()
        send = "Kurz: 1 {} = {} {}".format(base_currency, dat["data"][currencies], currencies)
        await message.channel.send(send)
    if message.content.lower().startswith("!timenow"):
        url = "http://worldtimeapi.org/api/timezone/Europe/Prague"
        rq = requests.get(url = url)
        data = rq.json()
        wkdays = {1: "pondělí", 2: "úterý", 3: "středa", 4: "čtvrtek", 5: "pátek", 6: "sobota", 7: "neděle"}
        info1 = "Informace o čase:"
        info2 = f"V Praze je momentálně {wkdays[data['day_of_week']]} {data['datetime'][0:10]} a čas je {data['datetime'][11:19]}."
        info3 = f"V roce je toto {data['day_of_year']}. den a {data['week_number']}. týden."
        info4 = ""
        info5 = ""
        if data['dst'] == True:
            info4 = "Evropská unie ještě Česku nenařídila ponechání letního/zimního času."
            if data['utc_offset'] == "+02:00":
                info5 = "Momentálně je tudíž letní čas."
            elif data['utc_offset'] == "+01:00":
                info5 = "Momentálně je tudíž zimní čas."
            else:
                info5 = "CHYBA: Špatné časové pásmo!"
        elif data['dst'] == False:
            if data['utc_offset'] == "+02:00":
                info5 = "Evropská unie Česku nařídila ponechání letního času."
            elif data['utc_offset'] == "+01:00":
                info5 = "Evropská unie Česku nařídila ponechání zimního času."
            else:
                info5 = "CHYBA: Špatné časové pásmo!"
        else:
            info4 = "CHYBA: nefunguje kontrola letního času."
        info6 = f"Pro programátory se hodí taky čas: {data['unixtime']}"
        info = info1 + "\n" + info2 + "\n" + info3 + "\n" + info4 + "\n" + info5 + "\n" + info6
        await message.channel.send(info)
client.run(TOKEN)